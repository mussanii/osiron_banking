import HeaderBox from '@/components/ui/HeaderBox'
import RightSidebar from '@/components/ui/RightSidebar'
import TotalBalanceBox from '@/components/ui/TotalBalanceBox'

const Home = () => {
  const loggedIn ={firstName:'Kevin',lastName:'Musanii',email:'kevin@osiron.io'}
  return (
    <section className='home'>
      <div className='home-content'>
        <header className='home-header'>
        <HeaderBox
        type="greeting"
        title="Welcome"
        user={loggedIn?.firstName || 'Guest'}
        subtext="Access and manage your account and transactions efficiently."
        />

        <TotalBalanceBox
        accounts={[]}
        totalBanks={1}
        totalCurrentBalance={1380.58}


        />
        </header>
        RECENT TRANSACTIONS
   

      </div>
      <RightSidebar user={loggedIn}
      transactions ={[]}
      banks = {[{ currentBalance:1380.56 },{currentBalance:1000.49}]}
      />
    

    </section>
  )
}

export default Home
