'use client';
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"

import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import CustomInput from './CustomInput';
import { AuthFormSchema } from '@/lib/utils';
import { Loader2 } from 'lucide-react';



const AuthForm = ({type}:{type:string}) => {
    
    const[user, setUser] = useState(null);
    const [isLoading, setIsLoading] = useState(false)

    const formSchema = AuthFormSchema(type);
    // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
    },
  })
 
  // 2. Define a submit handler.
  function onSubmit(values: z.infer<typeof formSchema>) {
    // Do something with the form values.
    // ✅ This will be type-safe and validated.
    setIsLoading(true)
    console.log(values)
    setIsLoading(false)
  }


  return (
    <section className="auth-form">
        <header className='flex flex-col gap-5 md:gap-8'>
        <Link href='/' className=' cursor-pointer flex items-center gap-1'>
                <Image
                src="/icons/logo.svg"
                width={34}
                height={34}
                alt='Osiron Logo'
               
                />
                <h1 className='text-26 font-ibm-plex-serif font-bold text-black-1'>Osiron</h1>
            </Link>
            <div className="flex flex-col gap-1 md:gap-3">
                <h1 className='text-24 lg:text-36 font-semibold text-gray-900'>
                    {
                    user ? 'Link Account'
                    : type ==='sign-in'
                    ? 'Sign In'
                    : 'Sign Up'
                }
                </h1>
                <p className="text-16 font-normal text-gray-600">
                    {
                    user ? 'Link your account to get started'
                         :'Please enter your details'
                }
                </p>

            </div>

        </header>
        {
            user ? (
                <div className="flex flex-col gap-4">
                    {/*PlaidLink */}
                </div>
            ):(
                <Form {...form}>
                <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
                  {type ==='sign-up' && (
                    <>

                    <div className='flex  gap-4'>
                      <CustomInput  control={form.control} name='firstName' label='First Name' type='text' placeholder='Enter your first name'/>

                      <CustomInput  control={form.control} name='lastName' label='Last Name' type='text' placeholder='Enter your last name'/>

                    </div>
                     
                     <CustomInput  control={form.control} name='address1' label='Address' type='text' placeholder='Enter your specific address'/>
                     <div className='flex  gap-4'>
                     <CustomInput  control={form.control} name='state' label='State' type='text' placeholder='Example: KE'/>

                     <CustomInput  control={form.control} name='postalCode' label='Postal Code' type='text' placeholder='Example: 0000'/>
                     </div>
                     <div className='flex  gap-4'>
                     <CustomInput  control={form.control} name='dateOfBirth' label='Date Of Birth' type='text' placeholder='YYYY-MM-DD'/>
                     
                     <CustomInput  control={form.control} name='ssn' label='SSN' type='text' placeholder='Example:1234'/>
                      </div>
                    </>
                  )}
                    <CustomInput  control={form.control} name='email' label='Email' type='text' placeholder='Enter your email'
                    />
                    <CustomInput control={form.control} name='password' label='Password' type='password' placeholder='Enter your password'
                    />
                 <div className=' flex flex-col gap-4'>
                 <Button className='form-btn' type="submit" disabled={isLoading}>
                    {isLoading ? (
                      <>
                      <Loader2 size={20} className='animate-spin'/> &nbsp; Loading ...
                      </>
                    ) : type ==='sign-in'
                    ? 'Sign In':'Sign Up' }
                    </Button>
                 </div>
                 
                </form>
                <footer className="flex justify-center gap-1">
              <p className='tex-14 font-normal text-gray-600'>
                {type==='sign-in'
                 ? "Don't have an account?"
                 : "Already have an account?"}
                 </p>
                 <Link href={type ==='sign-in' ? '/sign-up':'/sign-in'} className='form-link'>
                 {type ==='sign-in' ? 'Sign Up':'Sign In'}
                 </Link>
            </footer>
              
              </Form>
             
              
              
              
            )}



    </section>
     

    
  )
}

export default AuthForm
